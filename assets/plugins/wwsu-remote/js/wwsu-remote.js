"use strict";

// REQUIRES these WWSUmodules: hostReq (WWSUreq)
class WWSUremote extends WWSUevents {
	/**
	 * Construct the class
	 *
	 * @param {WWSUmodules} manager The modules class which initiated this module
	 * @param {object} options Options to be passed to this module
	 */
	constructor(manager, options) {
		super();

		this.manager = manager;

		this.endpoints = {
			request: "POST /api/call/start/:ID?",
			credentialComputer: "GET /api/call/credential",
			quality: "PUT /api/call/quality/:quality",
		};
		this.data = {
			request: {},
		};

		this.manager.socket.on("call-quality", (quality) => {
			this.emitEvent("callQuality", [quality]);
		});
	}

	/**
	 * Request for an audio call to the API so other DJ Controls loads its remote process
	 *
	 * @param {object} data Data to send to the API
	 * @param {?function} cb Callback after request is made
	 */
	request(data, cb) {
		this.manager
			.get("hostReq")
			.request(this.endpoints.request, { data }, {}, (body, resp) => {
				if (resp.statusCode >= 400) {
					if (typeof cb === "function") {
						cb(false);
					}
				} else {
					if (typeof cb === "function") {
						cb(true);
					}
				}
			});
	}

	/**
	 * Authorize a host for connecting to Skyway.js.
	 * TODO: Once the new DJ Controls is ready, credentials should be forced on the Skyway.js dashboard.
	 *
	 * @param {object} data Data to be passed to the API
	 * @param {?function} cb Callback which returns credential data on success
	 */
	credentialComputer(data, cb) {
		this.manager
			.get("hostReq")
			.request(
				this.endpoints.credentialComputer,
				{ data },
				{},
				(body, resp) => {
					if (
						resp.statusCode < 400 &&
						typeof body === "object" &&
						body.authToken
					) {
						if (typeof cb === "function") {
							cb(body);
						}
					} else {
						if (typeof cb === "function") {
							cb(false);
						}
					}
				}
			);
	}

	/**
	 * Send call quality data to WWSU API to be transmitted in sockets.
	 *
	 * @param {object} data Data to be passed to the API
	 */
	sendQuality(data) {
		this.manager
			.get("hostReq")
			.request(
				this.endpoints.quality,
				{ data },
				{ hideErrorToast: true },
				(body, resp) => {}
			);
	}
}
