class PureSnow {
	constructor(snowflakes_count = 200, base_id = `pure-snow`, base_css = ``) {
		this.snowflakes_count = snowflakes_count;
		this.base_css = base_css;
		this.base_id = base_id;

		this.active = false;
	}

	// This function allows you to turn on and off the snow
	toggle_snow(activate = true) {
		if (activate) {
			if (!this.active) {
				this._spawnSnowCSS();
				this._spawn_snow();
				$(`#${this.base_id}`).css("display", "block");
				this.active = true;
			}
		} else {
			if (this.active) {
				this._destroy_snow();
				$(`#${this.base_id}`).css("display", "none");
				this.active = false;
			}
		}
	}

	// Create snowflakes
	_spawn_snow(snow_density = this.snowflakes_count) {
		snow_density -= 1;

		for (let x = 0; x < snow_density; x++) {
			let board = document.createElement("div");
			board.className = `${this.base_id}-flake`;

			document.getElementById(this.base_id).appendChild(board);
		}
	}

	// Destroy snowflakes
	_destroy_snow() {
		$(`.${this.base_id}-flake`).remove();
		$(`.${this.base_id}-flake-css`).remove();
	}

	// Append style for each snowflake to the head
	_add_css(rule) {
		let css = document.createElement("style");
		css.className = `${this.base_id}-flake-css`;
		css.type = "text/css";
		css.appendChild(document.createTextNode(rule)); // Support for the rest
		document.getElementsByTagName("head")[0].appendChild(css);
	}

	random_int(value = 100) {
		return Math.floor(Math.random() * value) + 1;
	}

	random_range(min, max) {
		min = Math.ceil(min);
		max = Math.floor(max);
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	// Create style for snowflake
	_spawnSnowCSS(snow_density = this.snowflakes_count) {
		let snowflake_name = `${this.base_id}-flake`;
		let rule = ``;
		if (typeof this.base_css !== "undefined") {
			rule = this.base_css;
		}

		for (let i = 1; i < snow_density; i++) {
			let random_x = Math.random() * 100; // vw
			let random_offset = this.random_range(-100000, 100000) * 0.0001; // vw;
			let random_x_end = random_x + random_offset;
			let random_x_end_yoyo = random_x + random_offset / 2;
			let random_yoyo_time = this.random_range(30000, 80000) / 100000;
			let random_yoyo_y = random_yoyo_time * 100; // vh
			let random_scale = Math.random();
			let fall_duration = this.random_range(10, 30) * 1; // s
			let fall_delay = this.random_int(30) * -1; // s
			let opacity_ = Math.random();

			rule += `
        .${snowflake_name}:nth-child(${i}) {
            opacity: ${opacity_};
            transform: translate(${random_x}vw, -10px) scale(${random_scale});
            animation: fall-${i} ${fall_duration}s ${fall_delay}s linear infinite;
            z-index: 99999;
            pointer-events: none;
        }

        @keyframes fall-${i} {
            ${random_yoyo_time * 100}% {
                transform: translate(${random_x_end}vw, ${random_yoyo_y}vh) scale(${random_scale});
            }

            to {
                transform: translate(${random_x_end_yoyo}vw, 100vh) scale(${random_scale});
            }
            
        }
        `;
		}

		this._add_css(rule);
	}
}
